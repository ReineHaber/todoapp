﻿namespace ToDoApp.Entity
{
    public class CreateToDoDTO
    {
        public string ToDoTitle { get; set; }
        public string ToDoDescription { get; set; } = string.Empty;
    }
}
