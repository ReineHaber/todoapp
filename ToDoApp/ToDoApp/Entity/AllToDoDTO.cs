﻿namespace ToDoApp.Entity
{
    public class AllToDoDTO
    {
        public string ToDoTitle { get; set; }
        public string ToDoCreatedDate { get; set; }
        public bool ToDoIsChecked { get; set; }
    }
}
