﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Data.SqlTypes;

namespace ToDoApp.Entity
{

    public class User
    {
        [Key]
        public int UserId { get; set; }

        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string Email { get; set; }

        public List<ToDoNote> notes { get; set; }

    }
}
