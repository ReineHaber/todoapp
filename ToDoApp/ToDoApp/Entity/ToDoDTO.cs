﻿namespace ToDoApp.Entity
{
    public class ToDoDTO
    {
        public string ToDoTitle { get; set; }
        public string ToDoCreatedDate { get; set; }
        public string ToDoIsChecked { get; set; }
        public string ToDoDescription { get; set; } = string.Empty;
    }
}
