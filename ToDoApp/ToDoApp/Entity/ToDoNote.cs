﻿using System.ComponentModel.DataAnnotations;
namespace ToDoApp.Entity
{
    public class ToDoNote
    {
        [Key]
        public string ToDoId { get; set; }
        public string ToDoTitle { get; set;}
        public string ToDoDescription { get; set;} = string.Empty;
        public string ToDoCreatedDate { get; set; }
        public string ToDoIsChecked { get; set; }
        public ToDoNote() 
        { 
            ToDoId = Guid.NewGuid().ToString();
            ToDoCreatedDate = DateTime.Now.ToString("dd-MM-yyyy at HH-mm-ss");
            ToDoIsChecked = "false";
        }
    }
}
