﻿using Microsoft.EntityFrameworkCore;
using ToDoApp.Entity; 

namespace ToDoApp.Data
{
    public class TodoDBContext : DbContext 
    {
        public TodoDBContext(DbContextOptions options) : base(options)
        {

        }
        public DbSet<ToDoNote> TodoNotes { get; set; }
    }

}
