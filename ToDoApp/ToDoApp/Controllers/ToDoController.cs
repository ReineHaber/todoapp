﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Tools;
using ToDoApp.Data;
using ToDoApp.Entity;
using ToDoApp;
using System.Data.SQLite;
using Microsoft.EntityFrameworkCore;

namespace ToDoApp.Controllers
{
    [Route("api/v1")] 
    [ApiController]

    public class ToDoController : ControllerBase
    {
        public static List<ToDoNote> toDoNotes = new List<ToDoNote>();

        private readonly TodoDBContext todoDBContext;
        public ToDoController(TodoDBContext _todoDBContext)
        {
            todoDBContext = _todoDBContext;
        }
        //[Route("LogIn")]
        //[HttpGet]

        //public IActionResult LogIn([FromBody] string username, [FromBody] string password)
        //{
        //    var user = _dbContext.Users.FirstOrDefault(u=>u.UserName == username && u.Password == password);
        //    if (user != null)
        //    {
        //        return GetAll(user);
        //    }
        //    return NotFound();
        //}

        [Route("createNewToDo")]
        [HttpPost]

        public IActionResult create([FromBody] CreateToDoDTO createToDoDTO)
        {
            ToDoNote note = new()
            {
                ToDoTitle = createToDoDTO.ToDoTitle,
                ToDoDescription = createToDoDTO.ToDoDescription
            };

            toDoNotes.Add(note);
            addToDB(note);
           
            return Ok("To Do is created successfully!");
        }

        [Route("delete/{id}")]
        [HttpPost]

        public IActionResult delete([FromRoute]string id)
        {
            //higher order functions and callbacks
            foreach(ToDoNote note in  toDoNotes)
            {
                if(note.ToDoId == id)
                {
                    toDoNotes.Remove(note);
                    return Ok("the note was successfully deleted !");
                }
            }
            return NotFound();
        }

        //[Route("isChecked/{id}")]
        //[HttpPost]

        //public IActionResult changeToDoStatus([FromRoute]string id)
        //{
        //    foreach(var note in toDoNotes)
        //    {
        //        if(note.ToDoId == id)
        //        {
        //            note.ToDoIsChecked = !note.ToDoIsChecked;
        //            return Ok(note.ToDoIsChecked ? "Marked as finished" : "Marked as incomplete");
        //        }
        //    }
        //    return NotFound();
        //}

       


        [Route("{id}")]
        [HttpGet]

        public IActionResult GetById([FromRoute]string id)
        {
            foreach(var note in toDoNotes)
            {
                if(note.ToDoId == id)
                {
                    ToDoDTO toDoDTO = new()
                    {
                        ToDoCreatedDate = note.ToDoCreatedDate,
                        ToDoDescription = note.ToDoDescription,
                        ToDoIsChecked = note.ToDoIsChecked,
                        ToDoTitle = note.ToDoTitle
                    };
                    return Ok(toDoDTO);
                }
            }
            return NotFound("to-do not found !"); 
        }

        //[Route("all")]
        //[HttpGet]

        //public IActionResult GetAll(User user)
        //{
        //     List<AllToDoDTO> allDTO = new List<AllToDoDTO>();

        //    foreach(ToDoNote toDoNote in user.notes)
        //    {
        //        allDTO.Add(new AllToDoDTO()
        //        {
        //            ToDoCreatedDate = toDoNote.ToDoCreatedDate,
        //            ToDoIsChecked = toDoNote.ToDoIsChecked,
        //            ToDoTitle = toDoNote.ToDoTitle

        //        });
        //    }
        //    return Ok(allDTO);
        //}

        public IActionResult addToDB(ToDoNote note)
        {
            todoDBContext.TodoNotes.Add(note);
            todoDBContext.SaveChanges();

            return Ok();
        }

    }
}
