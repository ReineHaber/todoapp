﻿using System;
using ToDoApp.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.Sqlite;

namespace ToDoApp
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public TodoDBContext dbContext { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services) 
        {
            string connectionString = Configuration.GetConnectionString("DefaultConnection");

            services.AddControllers();
            services.AddRazorPages();
            services.AddDbContext<TodoDBContext>(options =>
            options.UseSqlite(connectionString));

            //services.AddScoped<TodoDBContext>();
        }


        public void Configure(IApplicationBuilder app)
        {
            app.UseRouting();
    
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }


}


